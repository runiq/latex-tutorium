Dies ist mein LaTeX-Tutorium, das ich am 15., 22. und 29.10.2012 gehalten habe.

Die wichtigsten Links
=====================

* `LaTeX-Dokumentation <https://bitbucket.org/runiq/latex-tutorium/src/tip/ressourcen.rst>`_
* `Tutorien 1, 2, 3 im PDF-Format <https://bitbucket.org/runiq/latex-tutorium/downloads>`_
* `Quellcode <https://bitbucket.org/runiq/latex-tutorium/src>`_
* `Quellcode zum Herunterladen <https://bitbucket.org/runiq/latex-tutorium/get/tip.zip>`_
* `Änderungen <https://bitbucket.org/runiq/latex-tutorium/changesets>`_

Falls ihr Anregungen oder Verbesserungsvorschläge habt, schreibt mir entweder eine Mail an patrice.peterson<at>student.uni-halle.de, oder öffnet einen entsprechenden `Bug-Report <https://bitbucket.org/runiq/latex-tutorium/issues/new>`_.

Herunterladen
=============

Die fertigen PDF-Dateien könnt ihr, wenn sie vorliegen, auf der `Download-Seite
<https://bitbucket.org/runiq/latex-tutorium/downloads>`_ dieses Repositoriums
herunterladen. Den neuesten Quellcode erhaltet ihr `hier
<https://bitbucket.org/runiq/latex-tutorium/get/tip.zip>`_.

Ich werde versuchen, die PDF-Dateien immer aktuell zu halten, wenn ich den
Quellcode update.

Nach und nach werden dort auch andere Sachen erscheinen; u.a. die anfänglichen und fertigen Versionen des zwergischen Forschungsberichts.

Code-Änderungen
===============

Ich werde nach und nach Änderungen am Dokument vornehmen, die ihr unter `dieser
Adresse <https://bitbucket.org/runiq/latex-tutorium/changesets>`_ verfolgen
könnt. Klickt dann einfach auf den Code des Commits (Commit =
"Änderungsschritt"), von dem ihr die Änderungen sehen wollt. Beispielsweise
habe ich in `diesem Commit
<https://bitbucket.org/runiq/latex-tutorium/changeset/e307466a806f55a7c2ee1b8b2b74b42e59b6f4fb>`_
ein paar Optionen für das ``hyperref``-Paket hinzugefügt (grün), und in `diesem <https://bitbucket.org/runiq/latex-tutorium/changeset/ca3f1f89c0975a94e8a2aab4503879d2c1c6f75f>`_ sowohl Sachen hinzugefügt als auch entfernt (rot).

Quellcode ansehen
=================

Für die Neugierigen gibt es in der Leiste mit dem `Source-Knopf
<https://bitbucket.org/runiq/latex-tutorium/src>`_ die Möglichkeit, sich den
Quellcode online anzusehen, ohne ihn herunterladen zu müssen.

Selbst kompilieren
==================

Wenn ihr den Quellcode heruntergeladen habt, könnt ihr ihn auch selbst kompilieren. Sofern ihr latexmk habt, geht das mit folgendem Befehl (Beispiel für `tutorium1.tex`)::

    latexmk -pdf -pdflatex=lualatex -shell-escape tutorium1.tex

Voraussetzungen
---------------

Zum Bauen der Dokumentation benötigt ihr folgende Pakete und ihre Abhängigkeiten:

* `beamer <http://ctan.org/pkg/beamer>`_
* `fontspec <http://ctan.org/pkg/fontspec>`_
* `babel <http://ctan.org/pkg/babel>`_
* `minted <http://ctan.org/pkg/minted>`_
* `mhchem <http://ctan.org/pkg/mhchem>`_

Außerdem sind weitere Sachen vonnöten:

* die Schriftarten `Linux Libertine und Linux Biolinum <http://www.linuxlibertine.org/index.php?id=91>`_ (frei und kostenlos
  erhältlich)
* die Schriftart `Consolas <https://www.microsoft.com/en-us/download/details.aspx?id=17879>`_ (kostenlos erhältlich)
* ein `Python-2-Interpreter <http://python.org/ftp/python/2.7.3/python-2.7.3.msi>`_ für Pygments
* das Programm `Pygments <http://pygments.org>`_ (für das ``minted``-Paket)

Mir ist bewusst, dass das Kompilieren des Dokuments mit diesen Abhängigkeiten
einige Schwierigkeiten bereiten kann. Ich werde mich nach anderen Schriften und
einem anderen Paket für die Syntaxhervorhebung umsehen, damit die Kompilierung
auch unter Windows reibungslos funktioniert.
