Doku-Übersicht
##############

Webseiten
=========

* `CTAN <http://ctan.org>`_, `CTAN-Suche <http://ctan.org/search>`_, `CTAN-Schlagwortsuche <http://ctan.org/http://ctan.org/keyword>`_
* `dante.de <http://www.dante.de>`_ und `Die TeXnische Komödie <http://www.dante.de/DTK.html>`_
* Wikibooks (`englisch <https://en.wikibooks.org/wiki/LaTeX>`_, `deutsch <https://de.wikibooks.org/wiki/LaTeX-Kompendium>`_)
* `l2kurz.pdf <http://mirror.ctan.org/info/lshort/german/l2kurz.pdf>`_ ("Kurze" Einführung [50 Seiten])
* `TeX Stackexchange <http://tex.stackexchange.com>`_

Symbole finden
==============

* `LaTeX Font Catalogue <http://www.tug.dk/FontCatalogue>`_: Schriftkatalog
* `Detexify² <http://detexify.kirelabs.org/classify.html>`_: Symbole malen, um deren Namen zu finden
* `AMS LaTeX Short Math Guide <ftp://ftp.ams.org/pub/tex/doc/amsmath/short-math-guide.pdf>`_: "Kurze" Einführung (20 Seiten) zu Mathe mit den ``amsmath``- bzw. ``mathtools``-Paketen
* `The Comprehensive LaTeX symbol list <http://www.ctan.org/tex-archive/info/symbols/comprehensive/symbols-letter.pdf>`_: Liste mit "allen" Symbolen, die in LaTeX gebräuchlich sind

Externe Werkzeuge zum Erstellen von Tabellen
============================================

* `calc2latex <http://calc2latex.sourceforge.net>`_ (OpenOffice/LibreOffice → LaTeX)
* `excel2latex <http://www.ctan.org/tex-archive/support/excel2latex>`_ (Excel → LaTeX)
* `matrix2latex <http://www.mathworks.com/matlabcentral/fileexchange/4894-matrix2latex>`_ (MATLAB → LaTeX)
* `latex-tools <http://rubygems.org/gems/latex-tools>`_ (Ruby-Bibliothek zum Erstellen von LaTeX-Tabellen)
* `xtable <http://cran.r-project.org/web/packages/xtable/index.html>`_ (R → LaTeX)
* `Online Table Generator <http://truben.no/latex/table>`_

Auf dem neuesten Stand bleiben
==============================

* `nag <http://ctan.org/pkg/nag>`_-Paket
* `9 Essential LaTeX packages <http://www.howtotex.com/packages/9-essential-latex-packages-everyone-should-use>`_ (nicht alle sind für KOMA-Script-Nutzer wirklich essenziell – z.B. ``geometry`` statt ``typearea``, ``amsmath`` statt ``mathtools``)
* `Pakete, die man immer brauchen kann <http://tex.stackexchange.com/questions/553>`_
* `How to keep up with good practices <http://tex.stackexchange.com/questions/19264>`_
* `How to keep up with new packages <http://tex.stackexchange.com/questions/3910>`_
* `Packages that need to be included in a specific order <http://tex.stackexchange.com/questions/3090>`_, `Which packages to load after hyperref <http://tex.stackexchange.com/questions/1863>`_ und `LaTeX package conflicts <http://www.macfreek.nl/memory/LaTeX_package_conflicts>`_

Schaufenster
============

* `texample.net <http://texample.net>`_
* `Schöne LaTeX-Dokumente <http://tex.stackexchange.com/questions/1319>`_
* `Lev S. Bishops Doktorarbeit – mit ausführlichen Erläuterungen, Quellcode, etc. <http://www.levbishop.org/thesis/source>`_ (Arbeit selbst `hier <http://www.levbishop.org/thesis/Bishop-thesis.pdf>`_)

LaTeX-Editoren
==============

* `LaTeX-Editoren <http://tex.stackexchange.com/questions/339>`_
* `Vergleich von 8 LaTeX-Editoren <http://www.charlietanksley.net/philtex/editors>`_
* `Warum LyX kein LaTeX-Editor im eigentlichen Sinne ist <http://tex.stackexchange.com/questions/28832>`_

Typografie
==========

* `l2tabu.pdf <http://mirror.ctan.org/info/l2tabu/german/l2tabu.pdf>`_
* `The Elements of Typographic Style <https://en.wikipedia.org/wiki/The_Elements_of_Typographic_Style>`_
* `Thinking With Type <http://www.thinkingwithtype.com>`_ (eine der schönsten Seiten, die ich kenne)

Anderes
=======

* `Vorteile von Versionskontrollsystemen beim Schreiben von LaTeX <http://tex.stackexchange.com/questions/1118>`_
* `Einführungsvideo zu Git<http://git-scm.com/video/what-is-version-control>`_, einem verteilten Versionskontrollsystem – leider nur englisch und etwas abstrakt, aber das beste und kürzeste, was ich finden konnte

Paketdokumentation
==================

* `KOMA-Script <http://mirror.ctan.org/macros/latex/contrib/koma-script/doc/scrguide.pdf>`_
* `amsmath <http://mirror.ctan.org/macros/latex/required/amslatex/math/amsldoc.pdf>`_, `mathtools <http://mirror.ctan.org/macros/latex/contrib/mh/mathtools.pdf>`_
* `babel <http://mirror.ctan.org/info/babel/babel.pdf>`_
* `biblatex <http://mirror.ctan.org/macros/latex/contrib/biblatex/doc/biblatex.pdf>`_
* `fontspec <http://mirror.ctan.org/macros/latex/contrib/fontspec/fontspec.pdf>`_
* `mhchem <http://mirror.ctan.org/macros/latex/contrib/mhchem/mhchem.pdf>`_, `chemfig <http://mirror.ctan.org/macros/generic/chemfig/chemfig_doc_en.pdf>`_, `chemmacros <http://mirror.ctan.org/macros/latex/contrib/chemmacros/chemmacros_de.pdf>`_, `chemnum <http://mirror.ctan.org/macros/latex/contrib/chemnum/chemnum_de.pdf>`_
* `nag <http://ctan.org/pkg/nag>`_
