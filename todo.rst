Technisches
###########

* `minted <http://ctan.org/pkg/minted>`_ durch `listings <http://ctan.org/pkg/listings>`_ und ein bisschen Formatiercode ersetzen
* Schriftpaket benutzen, das mit LaTeX mitgeliefert ist, über
  ``\setmainfont{TeX Gyre Pagella}``
* Bei den Themen Links auf die entsprechende Seite im LaTeX-Wikibook setzen
* Kann Beamer Anhangsfolien? Für:
    * Arten von Leerzeichen, Strichen
    * Typografische Feinheiten & Tabus
    * Doku-Übersicht

Überfällig vom letzten mal
##########################

Neue Befehle
============

* ``\providecommand`` (vs. ``\newcommand`` & ``\renewcommand``)
* ``\newenvironment``

Tutorium 3
##########

Seitenstile
===========

* Manuelle Formatierung (``\\``, ``\newpage``, ``\pagebreak``, ``\clearpage``
  etc.)

Tabellen & Abbildungen
======================

Gleitumgebungen
---------------

* `floatrow <http://ctan.org/pkg/floatrow>`_-Paket: Allg. Layoutverbesserung
  für Gleitumgebungen(?)
* `wrapfig <http://ctan.org/pkg/wrapfig>`_- und `floatflt
  <http://ctan.org/pkg/floatflt>`_-Pakete: Text umfließt Gleitumgebung
* `fltpage <http://ctan.org/pkg/fltpage>`_-Paket: Bildunterschriften von
  Abbildungen, die die ganze Seite einnehmen, werden auf eine angrenzende Seite
  gesetzt

Anhang
======

* typografische Feinheiten
    * Arten von Strichen (Binde- (Viertelgeviert-), Gedanken- (Halbgeviert-), englischer Gedankenstrich (Geviertstrich))
    * Worttrennung: `hyphsubst <http://ctan.org/pkg/hyphsubst>`_- und `dehyph-exptl <http://ctan.org/pkg/dehyph-exptl>`_-Pakete für neue
      experimentelle Trennregeln
    * ``\frenchspacing``
    * ``/`` vs. ``\slash`` in LaTeX-Dokumenten (wusste ich auch noch nicht…)
