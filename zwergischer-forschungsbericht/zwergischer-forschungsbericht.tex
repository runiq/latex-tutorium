\documentclass[draft,11pt]{scrartcl}
\usepackage{fontspec}
\defaultfontfeatures{Ligatures=TeX}

\usepackage[ngerman]{babel}

% Das todonotes-Paket erlaubt setzen von TODO-Kästchen
%
\usepackage[
% Zeigt "Liste der noch zu erledigenden Punkte" statt "To-do list" an
  ngerman,
% Zeigt die TODOs nur an, solange die "draft"-Option gesetzt ist.
  obeyDraft,
% Zeigt farbiges Kästchen in der TODO-Liste an, das auf den Typ des TODO hinweist
  colorinlistoftodos,
% Macht Schrift in TODO-Kästen kleiner
  textsize=footnotesize,
% Macht hässlichen schwarzen Rand weg
  bordercolor=none
  ]{todonotes}

% Einige neue Befehle für unterschiedliche Arten von TODOs:
% Orange für normale TODOs (Voreinstellung)...
% ...grüne Kästen für Verweise...
\newcommand{\todoref}[1]{\todo[color=green!50]{#1}}
% ...blaue Kästen für Literatur...
\newcommand{\todocite}[1]{\todo[color=blue!50]{#1}}
% ...rote Kästen für Fußnoten...
\newcommand{\todofn}[1]{\todo[color=red!50]{#1}}
% ...gelbe Kästen für Bilder...
\newcommand{\todofig}[1]{\todo[inline,color=yellow!50]{#1}}
% ...weiße Kästen für Tabellen...
\newcommand{\todotab}[1]{\todo[inline,color=white,bordercolor=black]{#1}}
% ...und blaue Kästen für Gleichungen
\newcommand{\todoeq}[1]{\todo[inline,color=blue!50]{#1}}

\begin{document}
\todototoc
\listoftodos
\clearpage

\todo{Seitenränder je 2,5\,cm}
\todo{Seitennummern oben mittig}

Titel: Die Auswirkung des Bolzengewichts auf die Leistungsfähigkeit von zwergischen Armbrüsten

Autor: Urist McZivilin

Datum: 31.August 2012

\todo{Titel, Autor, Datum formatieren}

\begin{abstract}
Das Ziel dieser Studie war es, eindeutig zu belegen, ob das Gewicht eines Bolzens (oder, genauer, die Dichte des Materials, aus dem der Bolzen hergestellt wurde) signifikante Auswirkungen auf die Leistungsfähigkeit von Armbrüsten hat. Es wurde rigoros wissenschaftlich gearbeitet, um Verbesserungen gegenüber gegenwärtigen experimentellen Messanordnungen zu gewährleisten. Die Autoren dieser Studie ziehen das Fazit, dass schwerere Bolzen eine höhere Durchschlagskraft besitzen, was diese gegen gepanzerte Ziele wertvoll macht.
\end{abstract}

\todo{Inhalts-, Abbildungs-, Tabellenverzeichnis}
\todo{Zeilenabstand 1,25-fach}

\section{Einleitung}

Eine nicht geringe Zahl von Studien ist bereits durchgeführt worden, um herauszufinden, ob das Gewicht eines abgeschossenen Bolzens Einfluss auf das Schadenspotenzial des ihn abfeuernden Schützenzwergs hat. Überraschenderweise haben diese Studien enorm unterschiedliche Ergebnisse zu Tage gebracht. Auf der einen Seite wurde von einer insignifikanten Steigerung der Feuerkraft von Armbrüsten berichtet (L)\todocite{It2011}: Truppen ausgerüstet mit schweren und leichten Bolzen waren unterschiedlich stark, wobei dies jedoch anderen, ungenannten Faktoren zugesprochen wurde. Laut einer neueren Studie (L)\todocite{Wrex2012} jedoch richten schwere Bolzen mehr Schaden an als leichte.

In dieser Arbeit wurde eine angemessene experimentelle Versuchsanordnung aufgebaut, zu dem Zweck, die Sache ein für alle mal zu klären.

\section{Methodik}

Der hier durchgeführte Versuch basiert auf dem in (L)\todocite{It2011} dargestellten: Eine 10x10-Matrix von Dreierkacheln, bei der jede Dreierkachel zwei Zwerge enthält, die durch ein Stück Festungsmauer voneinander getrennt sind (A)\todoref{Abbildung 1}.

\todofig{bilder/abb1.png: Ich weiß – die Kunst, unterschiedlich
  aussehende Zwerge zu entwerfen, entzieht sich mir.}

Diese elegante Versuchsanordnung ermöglicht es, die Ergebnisse von 100 isolierten, voneinander unabhängigen Versuchen parallel zu messen. Nach einem ersten Testlauf hat sich jedoch herausgestellt, dass es Probleme im Hinblick auf die zeitliche Abfolge des Experiments gab. Dies wird in (A)\todoref{Abbildung 2} deutlich, die die Matrix von oben 8 Sekunden nach Experimentsbeginn zeigt.

\todofig{bilder/abb2.png: 8 Sekunden nach Beginn des Experiments}

Wie eindeutig zu sehen ist, feuern nicht alle Zwerge gleichzeitig, sondern in Gruppen von 20 pro Sekunde. Dies bedeutet, dass einige Zwerge ihre Gegner \emph{überraschen}, was ernste Konsequenzen für die Reproduzierbarkeit des Experiments (F)\todofn{Und auch für die Reproduzierbarkeit der Zwergenpopulation, was hier jedoch als nebensächlich betrachtet werden soll.} haben kann, weil ein Bolzen, der auf einen überraschten Zwerg abgefeuert wird, diesen auf mehreren Wegen kampfunfähig machen kann:

\begin{itemize}
    \item Kopfschuss (sofortiges Ableben tritt ein)
    \item Armschuss (Armbrust/Munition wird fallengelassen; Experiment kann
      nicht fortgeführt werden)
    \item Extreme Schmerzen und Bewusstlosigkeit (erlauben dem Gegenüber, noch
      mehr Schüsse abzugeben)
\end{itemize}

Die Funktionsweise des Angriffsalgorithmus wäre in sich ebenfalls ein Thema für einen weiteren Artikel, würde in dieser Abhandlung jedoch zu weit gehen und soll nicht weiter ausgeführt werden.

Obwohl der Überraschungseffekt nicht so stark war wie erwartet, kam es doch zu Problemen bezüglich der starken Streuung (G)\todoref{Gleichung: Standardabweichung} der erhaltenen Rohdaten auf Grund der (relativ) geringen Anzahl redundanter Messungen. Auch die Messungen innerhalb der Kontrollgruppe, in der jede 3-Kachel zwei exakt gleich ausgerüstete Zwerge enthielt, resultierten häufig in Ergebnissen von 40:60 oder 60:40. Dies ist vergleichsweise weit entfernt vom postulierten 50:50-Szenario und würde bedeuten, dass auch bei einem 40:60-Ergebnis außerhalb der Kontrollgruppe keine eindeutige Schlussfolgerung gezogen werden kann, weil beim große Konfidenzintervall der Kontrollgruppe das hier vorgestellte Experiment nur eine geringe Mächtigkeit aufweist.\todo{Absatz: Relationen verschönern}

\todoeq{Standardabweichung:\\
SD (Sigma) = (Summe(1<=i<=n bis n) aller (z(i) - z-mit-balken)2) / n - 1}

Die finale Form der Versuchsanordnung sah letztendlich folgendermaßen aus:

\todotab{Tabelle 1: Ausrüstung nach Mannschaft}

``Leichtes Eisen''\todo{Deutsche Anführungszeichen} ist eine modifizierte Variante des normalen Metalls, das sich für die Belange dieses Experiments nur in der Dichte unterscheidet: Die Dichte von normalem Eisen beträgt 7,85 g/cm3, während die Dichte von ``leichtem Eisen''\todo{Deutsche Anführungszeichen} 0.2 g/cm3 beträgt. Sofern die Gewichtsformeln im Wiki (L)\todocite{Wiki2012} stimmen, wiegt ein Eisenbolzen etwa 1,17 Urist(F)\todofn{Das Urist ist noch keine anerkannte SI-Einheit. Wir arbeiten dran.}, während ein Bolzen aus leichtem Eisen 0,03 Urist wiegt.\todo{Absatz: SI-Einheiten}

In der ersten unserer experimentellen 10x10\todo{Relationen verschönern}-Matrizen befinden sich Truppe A und Truppe V, während die andere Truppe B und Truppe V beherbergt (siehe (A)\todoref{Abbildung 3}).  Truppe A resp.\ B tritt jeweils gegen Truppe V an. Da die Mitglieder von Truppe V unbewaffnet sind, wurden sie naturnotwendig niedergemetzelt, ohne Verluste auf der Gegenüberliegenden Seite. Das Kriterium, anhand dessen die Effektivität beider Truppen (A und B) gemessen wurde, war die durchschnittliche Zahl von Bolzen, die nötig war, um ein Mitglied von Truppe V auszulöschen. Dieser Wert wurde erhalten, indem nach Abschluss des Experiments der Bolzenköcher von jedem Mitglied von Truppe A und B inspiziert und die darin noch enthaltenen Bolzen gezählt wurden (G)\todoref{Wirkungsgrad}. Dieser Wert wurde dann von den anfänglichen 100 abgezogen. Je höher die durchschnittliche Anzahl von Bolzen war, die benötigt wurde, um ein Mitglied von Truppe V zu töten, umso weniger effektiv war das verwendete Material.

\todoeq{Wirkungsgrad: Am Gleichheitszeichen ausrichten\\
eta = b(verschossen) / b(gesamt) \\
b(gesamt) - b(Köcher) = b(verschossen) \\
Wirkungsgrad (eta) = b(gesamt) - b(Köcher) / b(gesamt)
}

\todofig{bilder/abb3.png: Versuchsanordnung v2.0. Das ``V'' steht für ``demnächst verwundet''.}\todo{Deutsche Anführungszeichen}

\todotab{Tabelle 2: Testläufe mit unterschiedlichen Rüstungen}

\section{Ergebnisse}

Vier Testläufe wurden durchgeführt, in jedem war Truppe V unterschiedlich gerüstet (siehe (T)\todoref{Tabelle 2}). Die erhaltenen Ergebnisse sind in (T)\todoref{Tabelle 3} dargestellt.

\todotab{Tabelle 3: Testergebnisse}

Außerdem gibt's ein paar lustige statistische Daten in Histogrammform in den (A)\todoref{Abbildung 4}.

\todofig{bilder/abb4.png: Erster Schwung Histogramme}

\section{Diskussion}

Die Auswirkungen des Bolzengewichts gegen ungerüstete Zwerge oder Zwerge mit Schilden sind vernachlässigbar gering. Beobachtete Unterschiede sind statistisch nicht signifikant.

Ein deutlicher Unterschied besteht jedoch in der Leistungsfähigkeit von Bolzen gegen gepanzerte Zwerge. Im Vergleich zu ihren Brüdern mit Eisenbolzen benötigten mit Bolzen aus leichtem Eisen ausgerüstete Schützenzwerge durchschnittlich doppelt so Schüsse, um einen gepanzerten, nicht mit Schild gerüsteten Zwerg zu Fall zu bringen; und 2,5-mal so viele gegen gepanzerte Zwerge \emph{mit} Schild. Interessanterweise waren die Zwerge in Truppe V, die als Opfer für Truppe B designiert waren, \emph{komplett unverletzt}: Die Zwerge aus Truppe B hatten ihre Bolzen aus leichtem Eisen komplett aufgebraucht (F)\todofn{Diese Zwerge werden in eventuellen Folgeversuchen mit Priorität der Truppe V zugeordnet.}.

Es kann also gefolgert werden, dass das Munitionsgewicht in der Tat bei der Bestimmung der Leistungskraft von Armbrüsten herangezogen werden muss, da es zur Rüstungspenetration beiträgt. Der Effekt ist praktisch nicht sichtbar gegen ungepanzerte Ziele, höchstwahrscheinlich auf Grund der guten Parameter der Armbrust selbst. Durch Schwächung dieser inhärent guten Parameter könnten neue Daten erhoben werden. Weitere zukünftige Arbeitsgebiete schließen Versuche mit unterschiedlichen Schärfegraden der Bolzenspitzen, Korrelationsversuche mit Schärfe und Gewicht, Tests mit handelsüblichen Materialien (im Gegensatz zu der hier verwendeten Eisenmodifikation) und den Einfluss der Fertigkeitsstufe des Schützen und des Verteidigers ein.

\todo{Literaturverzeichnis}
\end{document}
